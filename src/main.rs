use opencv::{prelude::*, highgui};
use opencv::core::{Point, Scalar, Size, Vector};
use opencv::dnn::TextDetectionModel_DB;
use opencv::highgui::imshow;
use opencv::imgcodecs::{imread, IMREAD_COLOR, imwrite};
use opencv::imgproc::{INTER_CUBIC, polylines, resize};


/// Create polygons around the area in an image that contains text
/// based on https://github.com/opencv/opencv/blob/master/doc/tutorials/dnn/dnn_text_spotting/dnn_text_spotting.markdown
fn main() -> anyhow::Result<()> {
    highgui::named_window("window", highgui::WINDOW_NORMAL)?;
    let mut img = imread("img.png", IMREAD_COLOR)?;

    let mut resized = Mat::default();
    let input_size = Size::new(736, 736);
    resize(&img, &mut resized, input_size, 0_f64, 0_f64, INTER_CUBIC)?;

    let net = opencv::dnn::read_net_from_onnx("DB_TD500_resnet50.onnx")?;
    let mut model = TextDetectionModel_DB::new(&net)?;
    model.set_binary_threshold(0.3)?
        .set_polygon_threshold(0.5)?
        .set_max_candidates(200)?
        .set_unclip_ratio(2.0)?;

    let scale = 1.0 / 255.0;
    let mean = Scalar::from((122.67891434, 116.66876762, 104.00698793));

    model.set_input_params(scale, input_size, mean, false, false)?;

    let mut det_results: Vector<Vector<Point>> = Vector::new();
    model.detect(&resized, &mut det_results)?;

    polylines(&mut resized, &det_results, true, Scalar::from((0.0, 255.0, 0.0)), 2, 1, 0)?;
    println!("ready");

    let orig_size = img.size()?;
    resize(&mut resized, &mut img, orig_size, 0_f64, 0_f64, INTER_CUBIC)?;
    imshow("window", &img)?;

    let mut params = Vector::new();
    params.push(0); //default strategy
    imwrite("result.png", &img, &params)?;

    loop {
        let key = highgui::wait_key(1)?;
        if key == 27 {
            break;
        }
    }

    Ok(())
}
